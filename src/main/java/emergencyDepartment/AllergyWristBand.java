/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package emergencyDepartment;

/**
 *
 * @author iuabd
 */
public class AllergyWristBand extends WristBand{
    
    //attribute(s)
    private String allergicMedicine;
    
    //getter(s)
    public String getAllergicMedicine() {
        return allergicMedicine;
    }
    
    //constructor(s)
    AllergyWristBand(String name, String dateOfBirth, String familyDoctor, String allergicMedicine){
        super(name, dateOfBirth, familyDoctor);
        this.allergicMedicine = allergicMedicine;
    }
}
