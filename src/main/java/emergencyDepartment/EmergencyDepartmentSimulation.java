/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package emergencyDepartment;

/**
 *
 * @author iuabd
 */
public class EmergencyDepartmentSimulation {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
       
       
       WristBand w = new WristBand("Tom", "10/10/1010", "Sherlock");
       ChildrenWristBand cw = new ChildrenWristBand("Jerry", "20/20/2020", "James", "Dad");
       AllergyWristBand aw = new AllergyWristBand("Molly", "30/30/3030", "Dolly", "Ibuprofen");
       
       System.out.println("\nPatient's name: "+w.getName() + 
               "\nPatient's Date of Birth: "+w.getDateOfBirth() + 
               "\nPatient's Family Doctor: "+w.getFamilyDoctor());
       
       System.out.println("\nPatient's name: "+cw.getName() + 
               "\nPatient's Date of Birth: "+cw.getDateOfBirth() + 
               "\nPatient's Family Doctor: "+cw.getFamilyDoctor() +
               "\nPatient's Parent: "+cw.getParentName());
       
       System.out.println("\nPatient's name: "+aw.getName() + 
               "\nPatient's Date of Birth: "+aw.getDateOfBirth() + 
               "\nPatient's Family Doctor: "+aw.getFamilyDoctor() +
               "\nPatient's Allegic to: "+aw.getAllergicMedicine());
    }
    
}
