/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package emergencyDepartment;

/**
 *
 * @author iuabd
 */
public class WristBand {
    
    //attribute(s)
    protected String name;
    protected String dateOfBirth;
    protected String familyDoctor;
       
    //getter(s)
    public String getName() {
        return name;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public String getFamilyDoctor() {
        return familyDoctor;
    }
    
    //constructor(s)
    public WristBand(String name, String dateOfBirth, String familyDoctor) {
        this.name = name;
        this.dateOfBirth = dateOfBirth;
        this.familyDoctor = familyDoctor;
    }
}
