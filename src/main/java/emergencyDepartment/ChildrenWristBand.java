/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package emergencyDepartment;

/**
 *
 * @author iuabd
 */
public class ChildrenWristBand extends WristBand{
    
    //attribute(s)
    private String parentName;
    
    //getter(s)
    public String getParentName() {
        return parentName;
    }
    
    //constructor(s)
    ChildrenWristBand(String name, String dateOfBirth, String familyDoctor, String parentName){
        super(name, dateOfBirth, familyDoctor);
        this.parentName = parentName;
    }
}
